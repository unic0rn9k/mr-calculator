//! [![gitlab]](https://gitlab.com/unic0rn9k/mr-calculator)
//! [![doc-rs]](https://unic0rn9k.gitlab.io/mr-calculator/doc/mrcalculator)
//! [![try-it]](https://unic0rn9k.gitlab.io/mr-calculator)
//! 
//! [gitlab]: https://img.shields.io/badge/gitlab-FC6D27?style=for-the-badge&labelColor=555555&logo=gitlab
//! [try-it]: https://img.shields.io/badge/try_it!-8da0cb?style=for-the-badge
//! [doc-rs]: https://img.shields.io/badge/docs.rs-66c2a5?style=for-the-badge&labelColor=555555&logoColor=white&logo=data:image/svg+xml;base64,PHN2ZyByb2xlPSJpbWciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDUxMiA1MTIiPjxwYXRoIGZpbGw9IiNmNWY1ZjUiIGQ9Ik00ODguNiAyNTAuMkwzOTIgMjE0VjEwNS41YzAtMTUtOS4zLTI4LjQtMjMuNC0zMy43bC0xMDAtMzcuNWMtOC4xLTMuMS0xNy4xLTMuMS0yNS4zIDBsLTEwMCAzNy41Yy0xNC4xIDUuMy0yMy40IDE4LjctMjMuNCAzMy43VjIxNGwtOTYuNiAzNi4yQzkuMyAyNTUuNSAwIDI2OC45IDAgMjgzLjlWMzk0YzAgMTMuNiA3LjcgMjYuMSAxOS45IDMyLjJsMTAwIDUwYzEwLjEgNS4xIDIyLjEgNS4xIDMyLjIgMGwxMDMuOS01MiAxMDMuOSA1MmMxMC4xIDUuMSAyMi4xIDUuMSAzMi4yIDBsMTAwLTUwYzEyLjItNi4xIDE5LjktMTguNiAxOS45LTMyLjJWMjgzLjljMC0xNS05LjMtMjguNC0yMy40LTMzLjd6TTM1OCAyMTQuOGwtODUgMzEuOXYtNjguMmw4NS0zN3Y3My4zek0xNTQgMTA0LjFsMTAyLTM4LjIgMTAyIDM4LjJ2LjZsLTEwMiA0MS40LTEwMi00MS40di0uNnptODQgMjkxLjFsLTg1IDQyLjV2LTc5LjFsODUtMzguOHY3NS40em0wLTExMmwtMTAyIDQxLjQtMTAyLTQxLjR2LS42bDEwMi0zOC4yIDEwMiAzOC4ydi42em0yNDAgMTEybC04NSA0Mi41di03OS4xbDg1LTM4Ljh2NzUuNHptMC0xMTJsLTEwMiA0MS40LTEwMi00MS40di0uNmwxMDItMzguMiAxMDIgMzguMnYuNnoiPjwvcGF0aD48L3N2Zz4K
//!
//! Mr. Calculator is a small static web app writen in rust, it compiles to wasm, and uses a cellular automata to simulate liquid.
//! The eastereggs are compile time evaluated bit maps

use macroquad::*;
use anyhow::*;

/// This is the square root of the amount of cells used for simulation.
/// It is also used for UI scaling, so other values may look off.
const SQUARES: usize = 100;

/// Function used for calculating movement of a cell of liquid over one frame.
fn liquid(n: usize, buffer: &[Color], buffer2: &[Color]) -> usize{
    let check_buffer = |n: usize| buffer[n] == BLACK && buffer2[n] == BLACK; // Check if N is a free cell in both buffers

    if (n + SQUARES) < buffer.len() && check_buffer(n + SQUARES){ // Check if cell can move down
        return n + SQUARES
    }

    let can_go_left  =  n % SQUARES > 0; // Check if cell is too close to the edge of the screen to move left
    let can_go_right = (n % SQUARES) < SQUARES - 1; // Same, but right
    
    if can_go_right && check_buffer(n + 1){ // Check if the right cell is free
        if can_go_left && check_buffer(n-1){ // Check if both the right, and the left are free
            return if rand::gen_range(0,2) == 0{n-1}else{n+1} // Pick a random direction to move
        }
        return n+1 // Move right
    }else if can_go_left && check_buffer(n-1){
        return n-1 // Move left
    }

    n // Don't move
}

/// ```
/// "1", "4", "7", "=",
/// "2", "5", "8", "0",
/// "3", "6", "9", "Delete",
/// "+", "-", "*", "/",
/// ```
const BUTTONS: [&str; 16] = [
    "1", "4", "7", "=",
    "2", "5", "8", "0",
    "3", "6", "9", "Delete",
    "+", "-", "*", "/",
];
const FACES  : [&str; 5] = [":3", ":O", ":<", ":,(", ":'<"];
const FRACES : [&str; 8] = [
    "Hi, I'm Mr. Calculator 95",
    "Fuck, that hurt!",
    "SHIIIIT!",
    "No don't! Please",
    "Oh God, the humanity",
    "HARDER DADDY UwU",
    "AAAAAAAAAAAAAAH!",
    "I don't feel so good Mr. stark",
];

/// Macro that can apply a string as a mathematical operator.
/// # Example
/// ```rust
/// assert!(math!(+, -, *, /, %)(1, "+", 2) == 3)
/// ```
macro_rules! math{
    ($($op: tt),*) => {{
        |a, b, c|{
            match b{
                $(
                    stringify!($op) => Ok(a $op c),
                )*
                c => Err(anyhow!("{} is not a known operation", c))
            }
        }
    }}
}

/// Macro that makes bitmaps for sprite rendering.
/// This is completely compiletime evaluated and just returns a [bool]
macro_rules! bitmap{
    ($($t:tt)*) => {{
        const T: u8 = 1;
        const F: u8 = 0;
        [$(
            $t == T
        ),*]
    }}
}

const SATAN: &[bool] = &bitmap!{
    F F F 1 1 1 1 1 F F F 
    F F 1 F 1 F F F 1 F F
    F 1 F F 1 F F 1 F 1 F 
    1 F F F 1 1 1 1 F F 1 
    1 1 1 1 F F 1 F F F 1 
    1 F F 1 F 1 1 F F F 1 
    F 1 F F 1 F 1 1 F 1 F 
    F F 1 F 1 F F 1 1 F F 
    F F F 1 1 F F F 1 F F 
    F F F F F 1 1 1 F F F 
};

const WEED: &[bool] = &bitmap!{
    F F F F F F 1 F F F F F
    F F F F F 1 F F F F F F
    F F F F F 1 F F F F F F
    F 1 F F 1 1 F F F F F F
    F 1 F F 1 1 F F F 1 F F
    F F 1 F 1 1 F F 1 F F F
    F F F 1 1 1 1 1 F F F F
    F 1 F F 1 1 1 F F 1 F F
    F F 1 1 1 1 1 1 1 F F F
    F F F F F 1 F F F F F F
    F F F F F 1 F F F F F F
};

const PORN: &[bool] = &bitmap!{
    F F F F F F F 1 1 1 1 1 1 1 1 F
    1 1 1 F F F 1 1 1 F 1 1 F 1 1 1
    1 F F 1 F F 1 1 1 F 1 1 F 1 1 1
    1 F F 1 F F 1 1 1 F 1 1 F 1 1 1
    1 1 1 F F F 1 1 1 F F F F 1 1 1
    1 F F F F F 1 1 1 F 1 1 F 1 1 1
    1 F F F F F 1 1 1 F 1 1 F 1 1 1
    F F F F F F F 1 1 1 1 1 1 1 1 F
};

/// Bad recursive function for evaluating simple math expresions. (It was very late when i wrote...
/// ***T H I S***)
fn parse_math(math: &str) -> Result<f64>{
    let math = math.replace(" ","");
    let mut branches = vec![];


    let mut check = |a: char, b: char|{ // Define closure for evaluate math from right to left
        for c in math.chars(){
            if c == a{
                branches = vec![];
                let mut args = math.split(a);
                branches.push(args.next().unwrap_or("").to_string());
                branches.push(a.to_string());
                branches.push(args.next().unwrap_or("").to_string());
            }else if c == b{
                branches = vec![];
                let mut args = math.split(b);
                branches.push(args.next().unwrap_or("").to_string());
                branches.push(b.to_string());
                branches.push(args.next().unwrap_or("").to_string());
            }
        }
    };
        
    check('*', '/'); // Parse higher order operations first.
    check('+', '-'); // This means lower order operations will be evaluated first.

    if branches.len() == 3{
        math!(+,-,*,/)(
            parse_math(&branches[0])?, // Recursively evaluate arguments,
                       &branches[1],   // this will calculate the value of the last expresions first,
            parse_math(&branches[2])?, // and will therfore reverse the order of evaluation.
        )
    }else{
        math.parse().context(math)
    }
}

/// Please don't be mad, I know it's bad.
#[macroquad::main("")]
async fn main() {
    let mut cells = [BLACK; SQUARES * SQUARES]; // Buffer containing cells for cellular automata
    let mut frace = 0; // Frace printed on screen
    let mut face = 0;  // Face  printed on screen
    let mut was_mouse_down = false;
    let mut math = "".to_string(); // buffer contatining math expression
    
    for x in 0..SQUARES{ // Loop over cells
        for y in 0..SQUARES{
            let edge = |x: usize, y: usize| x % (SQUARES / 4) == 0 || y % (SQUARES / 4) == 0; // Check if cell intersect with 4x4 grid

            if !(edge(x,y) || edge(x+1, y+1) || edge(x+2, y+2)){
                cells[x+y * SQUARES] = DARKGRAY // Main button color
            }else if edge(x, y) && !edge(x+1, y+1) && !edge(x+2, y+2){
                cells[x+y * SQUARES] = LIGHTGRAY // Button highlight color
            }
        }
    }

    loop {
        clear_background(BLACK);

        let game_size = screen_width().min(screen_height()); // Get smallest axis of screen
        let offset_x = (screen_width() - game_size) / 2. + 10.; // Calculate spacing between sides of screen and button grid
        let offset_y = (screen_height() - game_size) / 2. + 10.;
        let sq_size = (screen_height() - offset_y * 2.) / SQUARES as f32; // Calculate the size of one cell in pixels

        draw_text(
            &format!("{}   {}", FACES[face], FRACES[frace]),
            10.,
            10.,
            30.,
            WHITE,
        );
        draw_text(
            &format!("... {}", math),
            10.,
            40.,
            30.,
            WHITE,
        );

        let mut buffer = [BLACK; SQUARES * SQUARES]; // Buffer for next frame of cells

        let is_mouse_down = is_mouse_button_down(MouseButton::Left);
        if is_mouse_down && !was_mouse_down { // Check if mouse has been clicked (only runs once per click)
            face  = rand::gen_range(1, FACES.len()); // Set new face and frace
            frace = rand::gen_range(1, FRACES.len());
            
            let (x, y) = mouse_position();
            let mut x = ((x - offset_x) / sq_size) as usize; // Calculate which cells the mouse is on
            let mut y = ((y - offset_y) / sq_size) as usize;

            let mut n = x + y * SQUARES;
            while n < SQUARES * (SQUARES-1)-1 && cells[n] != BLACK{ // Loop creates rough line from mouse point down to the first blank area
                for dir in &[SQUARES as isize+2, SQUARES as isize-2, 1, -1]{ // Widen crease
                    let dir = dir + n as isize;
                    cells[dir as usize] = RED;
                }
                cells[n] = if rand::gen_range(0,2) == 1{RED}else{BLACK};
                cells[n+SQUARES] = RED;
                cells[n+1] = DARKBROWN; // Add shade to crease
                n = (n as i32 + rand::gen_range(-2, 2)) as usize + SQUARES; // Move line in down and randomly left/right
            }
            drop(n); // Free memory used for n

            x = x / (SQUARES / 4); // Which button are we on?
            y = y / (SQUARES / 4);
           
            if x < 4 && y < 4{ // Is it a valid button?
                match BUTTONS[y + x * 4]{
                    "Delete" => math = "".to_string(),
                    "=" => math = format!("{:?}", parse_math(&math)),
                    c => math += c,
                }
                
                let mut easteregg = |w, h, sx, sy, color, src: &[bool]|{
                    for x in 0..w{ // Loop over x and y for bitmap
                        for y in 0..h{
                            if src[x+y*w]{ // If the pixel is true, then set the color of the relative point in cell buffer
                                cells[(x+sx)+(y+sy)*SQUARES] = color
                            }
                        }
                    }
                };
                match &math[..]{ // Easteregss
                    "666" => {
                        math = "HAIL SATAN".to_string();
                        easteregg(11, 10, SQUARES/4+3, SQUARES/2+3, RED, &SATAN);
                    }
                    "420" => {
                        math = "SMOKE WEED".to_string();
                        easteregg(12, 11, SQUARES/2+2, SQUARES/2+3, DARKGREEN, &WEED);
                    }
                    "69" => {
                        math = "pornhub".to_string();
                        easteregg(16, 8, SQUARES/2+2, SQUARES/4+3, ORANGE, &PORN);
                    }
                    _ => {}
                }
            }
        }
        was_mouse_down = is_mouse_down;

        for x in 0..SQUARES{ // Draw cells
            for y in 0..SQUARES{
                let n = x + y * SQUARES;
                let p = |x: usize|x as f32 * sq_size;

                draw_rectangle(
                    p(x) + offset_x,
                    p(y) + offset_y,
                    sq_size,
                    sq_size,
                    cells[n]
                );

                if cells[n] == RED && n < SQUARES * (SQUARES-1){ // If the cell is red, it is a liquid.
                    buffer[liquid(n, &cells, &buffer)] = RED
                }else if cells[n] != BLACK{ // Else it is not
                    buffer[n] = cells[n];
                }
            }
        }

        let mut btn = 0;
        for x in 0..SQUARES{ // Draw text for buttons
            for y in 0..SQUARES{
                let p = |x: usize|x as f32 * sq_size;
                if x % (SQUARES / 4) == 0
                && y % (SQUARES / 4) == 0{
                    draw_text(
                        BUTTONS[btn],
                        p(x + 12 - BUTTONS[btn].len()) + offset_x,
                        p(y + 10) + offset_y,
                        sq_size * 4.,
                        WHITE,
                    );
                    btn += 1;
                }
            }
        }

        cells=buffer;
        next_frame().await
    }
}
