<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="API documentation for the Rust `glam` crate."><meta name="keywords" content="rust, rustlang, rust-lang, glam"><title>glam - Rust</title><link rel="stylesheet" type="text/css" href="../normalize.css"><link rel="stylesheet" type="text/css" href="../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../ayu.css" disabled ><script id="default-settings"></script><script src="../storage.js"></script><noscript><link rel="stylesheet" href="../noscript.css"></noscript><link rel="icon" type="image/svg+xml" href="../favicon.svg">
<link rel="alternate icon" type="image/png" href="../favicon-16x16.png">
<link rel="alternate icon" type="image/png" href="../favicon-32x32.png"><style type="text/css">#crate-search{background-image:url("../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 8]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu">&#9776;</div><a href='../glam/index.html'><div class='logo-container rust-logo'><img src='../rust-logo.png' alt='logo'></div></a><p class="location">Crate glam</p><div class="block version"><p>Version 0.8.7</p></div><div class="sidebar-elems"><a id="all-types" href="all.html"><p>See all glam's items</p></a><div class="block items"><ul><li><a href="#modules">Modules</a></li><li><a href="#structs">Structs</a></li><li><a href="#functions">Functions</a></li></ul></div><p class="location"></p><script>window.sidebarCurrent = {name: "glam", ty: "mod", relpath: "../"};</script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu"><img src="../brush.svg" width="18" alt="Pick another theme!"></button><div id="theme-choices" role="menu"></div></div><script src="../theme.js"></script><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" class="help-button">?</button>
                <a id="settings-menu" href="../settings.html"><img src="../wheel.svg" width="18" alt="Change settings"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../src/glam/lib.rs.html#1-167" title="goto source code">[src]</a></span><span class="in-band">Crate <a class="mod" href="">glam</a></span></h1><div class="docblock"><h1 id="glam" class="section-header"><a href="#glam">glam</a></h1>
<p><code>glam</code> is a simple and fast linear algebra library for games and graphics.</p>
<h2 id="features" class="section-header"><a href="#features">Features</a></h2>
<p><code>glam</code> is built with SIMD in mind. Currently only SSE2 on x86/x86_64 is
supported as this is what stable Rust supports.</p>
<ul>
<li>Single precision float (<code>f32</code>) support only</li>
<li>SSE2 implementation for most types, including <code>Mat2</code>, <code>Mat3</code>, <code>Mat4</code>, <code>Quat</code>,
<code>Vec3</code> and <code>Vec4</code></li>
<li>Scalar fallback implementations exist when SSE2 is not available</li>
<li>Most functionality includes unit tests and benchmarks</li>
</ul>
<h2 id="linear-algebra-conventions" class="section-header"><a href="#linear-algebra-conventions">Linear algebra conventions</a></h2>
<p><code>glam</code> interprets vectors as column matrices (also known as &quot;column vectors&quot;)
meaning when transforming a vector with a matrix the matrix goes on the left.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">glam</span>::{<span class="ident">Mat3</span>, <span class="ident">Vec3</span>};
<span class="kw">let</span> <span class="ident">m</span> <span class="op">=</span> <span class="ident">Mat3</span>::<span class="ident">identity</span>();
<span class="kw">let</span> <span class="ident">x</span> <span class="op">=</span> <span class="ident">Vec3</span>::<span class="ident">unit_x</span>();
<span class="kw">let</span> <span class="ident">v</span> <span class="op">=</span> <span class="ident">m</span> <span class="op">*</span> <span class="ident">x</span>;
<span class="macro">assert_eq</span><span class="macro">!</span>(<span class="ident">v</span>, <span class="ident">x</span>);</pre></div>
<p>Matrices are stored in memory in column-major order.</p>
<p>Rotations follow left-hand rule. The direction of the axis gives the direction
of rotation: with the left thumb pointing in the positive direction of the axis
the left fingers curl around the axis in the direction of the rotation.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">glam</span>::{<span class="ident">Mat3</span>, <span class="ident">Vec3</span>};
<span class="comment">// rotate +x 90 degrees clockwise around y giving -z</span>
<span class="kw">let</span> <span class="ident">m</span> <span class="op">=</span> <span class="ident">Mat3</span>::<span class="ident">from_rotation_y</span>(<span class="number">90.0_f32</span>.<span class="ident">to_radians</span>());
<span class="kw">let</span> <span class="ident">v</span> <span class="op">=</span> <span class="ident">m</span> <span class="op">*</span> <span class="ident">Vec3</span>::<span class="ident">unit_x</span>();
<span class="macro">assert</span><span class="macro">!</span>(<span class="ident">v</span>.<span class="ident">abs_diff_eq</span>(<span class="op">-</span><span class="ident">Vec3</span>::<span class="ident">unit_z</span>(), <span class="ident">core</span>::<span class="ident">f32</span>::<span class="ident">EPSILON</span>));</pre></div>
<h2 id="size-and-alignment-of-types" class="section-header"><a href="#size-and-alignment-of-types">Size and alignment of types</a></h2>
<p>Most <code>glam</code> types use SIMD for storage meaning most types are 16 byte aligned.
The only exception is Vec2`. When SSE2 is not available on the target
architecture the types will still be 16 byte aligned, so object sizes and
layouts will not change between architectures.</p>
<p>16 byte alignment means that some types will have a stride larger than their
size resulting in some wasted space.</p>
<table><thead><tr><th align="left">Type</th><th align="right">f32 bytes</th><th align="right">SIMD bytes</th><th align="right">Wasted bytes</th></tr></thead><tbody>
<tr><td align="left"><code>Vec3</code></td><td align="right">12</td><td align="right">16</td><td align="right">4</td></tr>
<tr><td align="left"><code>Mat3</code></td><td align="right">36</td><td align="right">48</td><td align="right">12</td></tr>
</tbody></table>
<p>Despite this wasted space the SIMD version tends to outperform the <code>f32</code>
implementation in <a href="https://github.com/bitshifter/mathbench-rs"><strong>mathbench</strong></a>
benchmarks.</p>
<p>SIMD support can be disabled entirely using the <code>scalar-math</code> feature. This
feature will also disable SIMD alignment meaning most types will use native
<code>f32</code> alignment of 4 bytes.</p>
<p>All the main <code>glam</code> types are tagged with #[repr(C)], so they are possible
to expose as struct members to C interfaces if desired. Be mindful of Vec3's
extra float though.</p>
<h2 id="accessing-internal-data" class="section-header"><a href="#accessing-internal-data">Accessing internal data</a></h2>
<p>The SIMD types that <code>glam</code> builds on are opaque and their contents are not
directly accessible. Because of this <code>glam</code> types uses getter and setter
methods instead of providing direct access.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">glam</span>::<span class="ident">Vec3</span>;
<span class="kw">let</span> <span class="kw-2">mut</span> <span class="ident">v</span> <span class="op">=</span> <span class="ident">Vec3</span>::<span class="ident">new</span>(<span class="number">1.0</span>, <span class="number">2.0</span>, <span class="number">3.0</span>);
<span class="macro">assert_eq</span><span class="macro">!</span>(<span class="ident">v</span>.<span class="ident">y</span>(), <span class="number">2.0</span>);
<span class="ident">v</span>.<span class="ident">set_z</span>(<span class="number">1.0</span>);
<span class="macro">assert_eq</span><span class="macro">!</span>(<span class="ident">v</span>.<span class="ident">z</span>(), <span class="number">1.0</span>);</pre></div>
<p>If you need to access multiple elements it is easier to convert the type to a
tuple or array:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">glam</span>::<span class="ident">Vec3</span>;
<span class="kw">let</span> <span class="ident">v</span> <span class="op">=</span> <span class="ident">Vec3</span>::<span class="ident">new</span>(<span class="number">1.0</span>, <span class="number">2.0</span>, <span class="number">3.0</span>);
<span class="kw">let</span> (<span class="ident">x</span>, <span class="ident">y</span>, <span class="ident">z</span>) <span class="op">=</span> <span class="ident">v</span>.<span class="ident">into</span>();
<span class="macro">assert_eq</span><span class="macro">!</span>((<span class="ident">x</span>, <span class="ident">y</span>, <span class="ident">z</span>), (<span class="number">1.0</span>, <span class="number">2.0</span>, <span class="number">3.0</span>));</pre></div>
<h2 id="simd-and-scalar-consistency" class="section-header"><a href="#simd-and-scalar-consistency">SIMD and scalar consistency</a></h2>
<p><code>glam</code> types implement <code>serde</code> <code>Serialize</code> and <code>Deserialize</code> traits to ensure
that they will serialize and deserialize exactly the same whether or not
SIMD support is being used.</p>
<p>The SIMD versions implement <code>core::fmt::Display</code> traits so they print the same as
the scalar version.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered">
<span class="kw">use</span> <span class="ident">glam</span>::<span class="ident">Vec3</span>;
<span class="kw">let</span> <span class="ident">a</span> <span class="op">=</span> <span class="ident">Vec3</span>::<span class="ident">new</span>(<span class="number">1.0</span>, <span class="number">2.0</span>, <span class="number">3.0</span>);
<span class="macro">assert_eq</span><span class="macro">!</span>(<span class="macro">format</span><span class="macro">!</span>(<span class="string">&quot;{}&quot;</span>, <span class="ident">a</span>), <span class="string">&quot;[1, 2, 3]&quot;</span>);</pre></div>
<h2 id="feature-gates" class="section-header"><a href="#feature-gates">Feature gates</a></h2>
<p>All <code>glam</code> dependencies are optional, however some are required for tests
and benchmarks.</p>
<ul>
<li><code>std</code> - the default feature, has no dependencies.</li>
<li><code>rand</code> - used to generate random values. Used in benchmarks.</li>
<li><code>serde</code> - used for serialization and deserialization of types.</li>
<li><code>mint</code> - used for interoperating with other linear algebra libraries.</li>
<li><code>scalar-math</code> - disables SIMD support and uses native alignment for all
types.</li>
<li><code>debug-glam-assert</code> - adds assertions in debug builds which check the validity
of parameters passed to <code>glam</code> to help catch runtime errors.</li>
<li><code>glam-assert</code> - adds assertions to all builds which check the validity of
parameters passed to <code>glam</code> to help catch runtime errors.</li>
</ul>
</div><h2 id="modules" class="section-header"><a href="#modules">Modules</a></h2>
<table><tr class="module-item"><td><a class="mod" href="f32/index.html" title="glam::f32 mod">f32</a></td><td class="docblock-short"></td></tr></table><h2 id="structs" class="section-header"><a href="#structs">Structs</a></h2>
<table><tr class="module-item"><td><a class="struct" href="struct.Mat2.html" title="glam::Mat2 struct">Mat2</a></td><td class="docblock-short"><p>A 2x2 column major matrix.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Mat3.html" title="glam::Mat3 struct">Mat3</a></td><td class="docblock-short"><p>A 3x3 column major matrix.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Mat4.html" title="glam::Mat4 struct">Mat4</a></td><td class="docblock-short"><p>A 4x4 column major matrix.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Quat.html" title="glam::Quat struct">Quat</a></td><td class="docblock-short"><p>A quaternion representing an orientation.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec2.html" title="glam::Vec2 struct">Vec2</a></td><td class="docblock-short"><p>A 2-dimensional vector.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec2Mask.html" title="glam::Vec2Mask struct">Vec2Mask</a></td><td class="docblock-short"><p>A 2-dimensional vector mask.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec3.html" title="glam::Vec3 struct">Vec3</a></td><td class="docblock-short"><p>A 3-dimensional vector.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec3Mask.html" title="glam::Vec3Mask struct">Vec3Mask</a></td><td class="docblock-short"><p>A 3-dimensional vector mask.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec4.html" title="glam::Vec4 struct">Vec4</a></td><td class="docblock-short"><p>A 4-dimensional vector.</p>
</td></tr><tr class="module-item"><td><a class="struct" href="struct.Vec4Mask.html" title="glam::Vec4Mask struct">Vec4Mask</a></td><td class="docblock-short"></td></tr></table><h2 id="functions" class="section-header"><a href="#functions">Functions</a></h2>
<table><tr class="module-item"><td><a class="fn" href="fn.mat2.html" title="glam::mat2 fn">mat2</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.mat3.html" title="glam::mat3 fn">mat3</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.mat4.html" title="glam::mat4 fn">mat4</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.quat.html" title="glam::quat fn">quat</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.vec2.html" title="glam::vec2 fn">vec2</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.vec3.html" title="glam::vec3 fn">vec3</a></td><td class="docblock-short"></td></tr><tr class="module-item"><td><a class="fn" href="fn.vec4.html" title="glam::vec4 fn">vec4</a></td><td class="docblock-short"></td></tr></table></section><section id="search" class="content hidden"></section><section class="footer"></section><script>window.rootPath = "../";window.currentCrate = "glam";</script><script src="../main.js"></script><script defer src="../search-index.js"></script></body></html>