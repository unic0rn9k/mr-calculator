var N = null;var sourcesIndex = {};
sourcesIndex["adler32"] = {"name":"","files":["lib.rs"]};
sourcesIndex["anyhow"] = {"name":"","files":["backtrace.rs","chain.rs","context.rs","error.rs","fmt.rs","kind.rs","lib.rs","macros.rs","wrapper.rs"]};
sourcesIndex["approx"] = {"name":"","files":["abs_diff_eq.rs","lib.rs","macros.rs","relative_eq.rs","ulps_eq.rs"]};
sourcesIndex["bitflags"] = {"name":"","files":["lib.rs"]};
sourcesIndex["byteorder"] = {"name":"","files":["io.rs","lib.rs"]};
sourcesIndex["cfg_if"] = {"name":"","files":["lib.rs"]};
sourcesIndex["crc32fast"] = {"name":"","dirs":[{"name":"specialized","files":["mod.rs","pclmulqdq.rs"]}],"files":["baseline.rs","combine.rs","lib.rs","table.rs"]};
sourcesIndex["deflate"] = {"name":"","files":["bit_reverse.rs","bitstream.rs","chained_hash_table.rs","checksum.rs","compress.rs","compression_options.rs","deflate_state.rs","encoder_state.rs","huffman_lengths.rs","huffman_table.rs","input_buffer.rs","length_encode.rs","lib.rs","lz77.rs","lzvalue.rs","matching.rs","output_writer.rs","rle.rs","stored_block.rs","writer.rs","zlib.rs"]};
sourcesIndex["glam"] = {"name":"","dirs":[{"name":"f32","files":["funcs.rs","mat2.rs","mat3.rs","mat4.rs","mod.rs","quat.rs","vec2.rs","vec2_mask.rs","vec3.rs","vec3_mask.rs","vec4.rs","vec4_mask.rs","x86_utils.rs"]}],"files":["lib.rs","macros.rs"]};
sourcesIndex["image"] = {"name":"","dirs":[{"name":"imageops","files":["affine.rs","colorops.rs","mod.rs","sample.rs"]},{"name":"io","files":["free_functions.rs","mod.rs","reader.rs"]},{"name":"math","files":["mod.rs","nq.rs","rect.rs","utils.rs"]},{"name":"tga","files":["decoder.rs","mod.rs"]},{"name":"utils","files":["mod.rs"]}],"files":["animation.rs","buffer.rs","color.rs","dynimage.rs","flat.rs","image.rs","lib.rs","png.rs","traits.rs"]};
sourcesIndex["inflate"] = {"name":"","files":["checksum.rs","lib.rs","reader.rs","utils.rs","writer.rs"]};
sourcesIndex["libc"] = {"name":"","dirs":[{"name":"unix","dirs":[{"name":"linux_like","dirs":[{"name":"linux","dirs":[{"name":"musl","dirs":[{"name":"b64","dirs":[{"name":"x86_64","files":["align.rs","mod.rs"]}],"files":["mod.rs"]}],"files":["mod.rs"]}],"files":["align.rs","mod.rs"]}],"files":["mod.rs"]}],"files":["align.rs","mod.rs"]}],"files":["fixed_width_ints.rs","lib.rs","macros.rs"]};
sourcesIndex["macroquad"] = {"name":"","files":["camera.rs","drawing.rs","exec.rs","lib.rs","models.rs","shapes.rs","texture.rs","time.rs","types.rs","ui.rs"]};
sourcesIndex["macroquad_macro"] = {"name":"","files":["lib.rs"]};
sourcesIndex["megaui"] = {"name":"","dirs":[{"name":"ui","files":["cursor.rs","input.rs"]},{"name":"widgets","dirs":[{"name":"editbox","files":["text_editor.rs"]}],"files":["button.rs","editbox.rs","group.rs","input_field.rs","label.rs","separator.rs","slider.rs","tabbar.rs","texture.rs","tree_node.rs","window.rs"]}],"files":["draw_command.rs","draw_list.rs","input_handler.rs","lib.rs","style.rs","types.rs","ui.rs","widgets.rs"]};
sourcesIndex["miniquad"] = {"name":"","dirs":[{"name":"graphics","files":["texture.rs"]}],"files":["clipboard.rs","conf.rs","event.rs","fs.rs","graphics.rs","lib.rs","log.rs"]};
sourcesIndex["miniquad_text_rusttype"] = {"name":"","files":["lib.rs"]};
sourcesIndex["mrcalculator"] = {"name":"","files":["main.rs"]};
sourcesIndex["num_integer"] = {"name":"","files":["average.rs","lib.rs","roots.rs"]};
sourcesIndex["num_iter"] = {"name":"","files":["lib.rs"]};
sourcesIndex["num_rational"] = {"name":"","files":["lib.rs"]};
sourcesIndex["num_traits"] = {"name":"","dirs":[{"name":"ops","files":["checked.rs","inv.rs","mod.rs","mul_add.rs","overflowing.rs","saturating.rs","wrapping.rs"]}],"files":["bounds.rs","cast.rs","float.rs","identities.rs","int.rs","lib.rs","macros.rs","pow.rs","real.rs","sign.rs"]};
sourcesIndex["ordered_float"] = {"name":"","files":["lib.rs"]};
sourcesIndex["png"] = {"name":"","dirs":[{"name":"decoder","files":["mod.rs","stream.rs"]}],"files":["chunk.rs","common.rs","encoder.rs","filter.rs","lib.rs","traits.rs","utils.rs"]};
sourcesIndex["quad_gl"] = {"name":"","files":["lib.rs"]};
sourcesIndex["quad_rand"] = {"name":"","files":["fy.rs","lib.rs"]};
sourcesIndex["rusttype"] = {"name":"","files":["lib.rs"]};
sourcesIndex["sapp_linux"] = {"name":"","files":["clipboard.rs","gl.rs","lib.rs","rand.rs","x.rs","x_cursor.rs","xi_input.rs"]};
sourcesIndex["stb_truetype"] = {"name":"","files":["lib.rs"]};
createSourceSidebar();
