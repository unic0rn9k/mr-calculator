(function() {var implementors = {};
implementors["anyhow"] = [{"text":"impl DerefMut for Error","synthetic":false,"types":[]}];
implementors["image"] = [{"text":"impl&lt;P, Container&gt; DerefMut for ImageBuffer&lt;P, Container&gt; <span class=\"where fmt-newline\">where<br>&nbsp;&nbsp;&nbsp;&nbsp;P: Pixel + 'static,<br>&nbsp;&nbsp;&nbsp;&nbsp;P::Subpixel: 'static,<br>&nbsp;&nbsp;&nbsp;&nbsp;Container: Deref&lt;Target = [P::Subpixel]&gt; + DerefMut,&nbsp;</span>","synthetic":false,"types":[]}];
implementors["ordered_float"] = [{"text":"impl&lt;T:&nbsp;Float&gt; DerefMut for OrderedFloat&lt;T&gt;","synthetic":false,"types":[]}];
if (window.register_implementors) {window.register_implementors(implementors);} else {window.pending_implementors = implementors;}})()