(function() {var implementors = {};
implementors["megaui"] = [{"text":"impl Into&lt;String&gt; for Color","synthetic":false,"types":[]}];
implementors["num_rational"] = [{"text":"impl&lt;T&gt; Into&lt;(T, T)&gt; for Ratio&lt;T&gt;","synthetic":false,"types":[]}];
implementors["ordered_float"] = [{"text":"impl Into&lt;f32&gt; for OrderedFloat&lt;f32&gt;","synthetic":false,"types":[]},{"text":"impl Into&lt;f64&gt; for OrderedFloat&lt;f64&gt;","synthetic":false,"types":[]}];
implementors["quad_gl"] = [{"text":"impl Into&lt;[f32; 4]&gt; for Color","synthetic":false,"types":[]},{"text":"impl Into&lt;([f32; 3], [f32; 2], [f32; 4])&gt; for Vertex","synthetic":false,"types":[]},{"text":"impl Into&lt;Vertex&gt; for VertexInterop","synthetic":false,"types":[]}];
if (window.register_implementors) {window.register_implementors(implementors);} else {window.pending_implementors = implementors;}})()