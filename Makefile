run:
	-make build
	qute localhost:4000

build:
	cargo b -j8 --release --target wasm32-unknown-unknown
	mv target/wasm32-unknown-unknown/release/mrcalculator.wasm ./public/main.wasm

doc:
	cargo doc -j8 -p mrcalculator
	cp -rf target/doc public/
	cargo readme > README.md

clean:
	cargo clean
	rm -rf public/main.wasm public/doc

init:
	basic-http-server ./public
